scalaVersion := "2.13.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test"
libraryDependencies += "org.mockito" %% "mockito-scala-scalatest" % "1.12.0" % "test"


mainClass in(Compile, run) := Some("net.mathant.game.cli.UserInteraction")