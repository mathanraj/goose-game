package net.mathant.game

import org.mockito.scalatest.MockitoSugar
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class GameSpec extends AnyFreeSpec with Matchers with MockitoSugar {

  "Players move" - {
    "when game has one player and that player is moved" - {
      "system should respond with correct board position" in {
        val pippo = Player("Pippo")
        val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
        val result = game.move(PlayerPosition(pippo), DiceOutcome(4, 3))

        result.playerPosition.currentSpace.position shouldBe 7
        result.gameResponse() shouldBe "Pippo rolls 4, 3. Pippo moves from Start to 7"
      }
    }

    "when game has two players and players are moved alternatively" - {
      "system should respond with correct board position for each move/player" in {

        val pippo = Player("Pippo")
        val pluto = Player("Pluto")
        val game = Game(NilPlayers.addPlayer(pippo).players.addPlayer(pluto).players, GameDice)
        val pippoMove1Response = game.move(PlayerPosition(pippo), DiceOutcome(5, 3))
        pippoMove1Response.playerPosition.currentSpace.position shouldBe 8
        pippoMove1Response.gameResponse() shouldBe "Pippo rolls 5, 3. Pippo moves from Start to 8"


        val plutoMove1Response = game.move(PlayerPosition(pluto), DiceOutcome(2, 2))
        plutoMove1Response.playerPosition.currentSpace.position shouldBe 4
        plutoMove1Response.gameResponse() shouldBe "Pluto rolls 2, 2. Pluto moves from Start to 4"

        val pippoMove2Response = game.move(pippoMove1Response.playerPosition, DiceOutcome(2, 3))
        pippoMove2Response.playerPosition.currentSpace.position shouldBe 13
        pippoMove2Response.gameResponse() shouldBe "Pippo rolls 2, 3. Pippo moves from 8 to 13"

      }
    }

    "when game has two players and another player not in game is trying to move" - {
      "system should respond with invalid move" in {

        val pippo = Player("Pippo")
        val pluto = Player("Pluto")
        val invalidPlayer = Player("InvalidPlayer")
        val game = Game(NilPlayers.addPlayer(pippo).players.addPlayer(pluto).players, GameDice)
        val invalidMove = game.move(PlayerPosition(invalidPlayer), DiceOutcome(4, 2))

        invalidMove.gameResponse() shouldBe "InvalidPlayer is not a valid player for this game!"
      }
    }

    "when game has one player and  player is trying to move with invalid dice outcome" - {
      "system should respond with invalid move" in {

        val pippo = Player("Pippo")
        val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
        val invalidMove = game.move(PlayerPosition(pippo), DiceOutcome(7, 3))

        invalidMove.gameResponse() shouldBe "For player: Pippo dice roll: (7, 3) is not a valid dice roll for this game!"
      }
    }
  }

  "Player wins" - {
    "when there is one player in space 60 and shoots exact dice to get 63" - {
      "system should declare the player as winner" in {
        val pippo = Player("Pippo")
        val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
        val result = game.move(PlayerPosition(pippo, NormalSpace(60)), DiceOutcome(1, 2))

        result.playerPosition.currentSpace.position shouldBe 63
        result.gameResponse() shouldBe "Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!"
      }
    }

    "when there is one player in space 60 and shoots dice to get (3,2))" - {
      "system should bounce back the player to 61" in {
        val pippo = Player("Pippo")
        val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
        val result = game.move(PlayerPosition(pippo, NormalSpace(60)), DiceOutcome(3, 2))

        result.playerPosition.currentSpace.position shouldBe 61
        result.gameResponse() shouldBe "Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61"
      }
    }
  }

  "The game throws the dice" - {
    "the player at position 4 want the system to throw dice" - {
      "the system throws dice, get (1,2) and should make move to position 7" in {
        val pippo = Player("Pippo")
        val dice = mock[Dice]
        when(dice.roll()).thenReturn(DiceOutcome(1, 2))
        when(dice.isInvalidDiceRoll(any[DiceOutcome])).thenReturn(false)

        val game = Game(NilPlayers.addPlayer(pippo).players, dice)
        val result: GameBoardResponse = game.move(PlayerPosition(pippo, NormalSpace(4)))
        result.game.playerPositions.find(_.player == pippo).get.currentSpace.position shouldBe 7
        result.gameResponse() shouldBe "Pippo rolls 1, 2. Pippo moves from 4 to 7"
      }
    }
  }

  "Space '6' is 'The Bridge'" - {
    "the player at position 4 throws dice and get (1,1)" - {
      "the system should move to 'The Bridge' at 6 and jump to position 12" in {
        val pippo = Player("Pippo")
        val dice = mock[Dice]
        when(dice.roll()).thenReturn(DiceOutcome(1, 1))
        when(dice.isInvalidDiceRoll(any[DiceOutcome])).thenReturn(false)

        val game = Game(NilPlayers.addPlayer(pippo).players, dice)
        val result = game.move(PlayerPosition(pippo, NormalSpace(4)))
        result.game.playerPositions.find(_.player == pippo).get.currentSpace.position shouldBe 12
        result.gameResponse() shouldBe "Pippo rolls 1, 1. Pippo moves from 4 to The Bridge. Pippo jumps to 12"

      }
    }
  }

  "If player land on 'The Goose', move again" - {
    "Single Jump" - {
      "the player at position 3, throws dice and get (1,1)" - {
        "system should move to 'the goose' and should move again by same dice outcome" in {

          val pippo = Player("Pippo")
          val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
          val result = game.move(PlayerPosition(pippo, NormalSpace(3)), DiceOutcome(1, 1))

          result.playerPosition.currentSpace.position shouldBe 7
          result.gameResponse() shouldBe "Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7"
        }
      }

      "the player at position 11, throws dice and get (5,2)" - {
        "system should move to 'the goose' and should move again by same dice outcome" in {

          val pippo = Player("Pippo")
          val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
          val result = game.move(PlayerPosition(pippo, NormalSpace(11)), DiceOutcome(5, 2))

          result.playerPosition.currentSpace.position shouldBe 25
          result.gameResponse() shouldBe "Pippo rolls 5, 2. Pippo moves from 11 to 18, The Goose. Pippo moves again and goes to 25"
        }
      }
    }
    "Multiple Jump" - {
      "2 jumps: the player at position 10, throws dice and get (2,2)" - {
        "system should move to 'the goose' by same dice outcome UNTIL the player in moved to non goose space" in {

          val pippo = Player("Pippo")
          val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
          val result = game.move(PlayerPosition(pippo, NormalSpace(10)), DiceOutcome(2, 2))

          result.playerPosition.currentSpace.position shouldBe 22
          result.gameResponse() shouldBe "Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 22"
        }
      }

      "3 jumps: the player at position 0, throws dice and get (4,5)" - {
        "system should move to 'the goose' by same dice outcome UNTIL the player in moved to non goose space" in {

          val pippo = Player("Pippo")
          val game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
          val result = game.move(PlayerPosition(pippo, StartingSpace), DiceOutcome(4, 5))

          result.playerPosition.currentSpace.position shouldBe 36
          result.gameResponse() shouldBe "Pippo rolls 4, 5. Pippo moves from Start to 9, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 27, The Goose. Pippo moves again and goes to 36"
        }
      }
    }
  }

  "Prank" - {
    "2 players: when a player Pippo land on a space occupied by another player Pluto" - {
      "the system sends Pluto back to Pippo's previous position" in {

        val pippo = Player("Pippo")
        val pluto = Player("Pluto")
        val players = NilPlayers.addPlayer(pippo).players.addPlayer(pluto).players
        val playerPositions = List(PlayerPosition(pippo, NormalSpace(15)), PlayerPosition(pluto, NormalSpace(17)))
        val game = Game(players, GameDice, playerPositions)
        val result = game.move(pippo, DiceOutcome(1, 1))

        result.gameResponse() shouldBe "Pippo rolls 1, 1. Pippo moves from 15 to 17. On 17 there is Pluto, who returns to 15"
      }
    }
    //Rare edge case where one player occupies the space and another player gets bounced back to same space
    "3 players: when a player Mario land on a space occupied by another player Pippo and Pluto" - {
      "the system sends Pippo and Pluto back to Mario's previous position" in {

        val pippo = Player("Pippo")
        val mario = Player("Mario")
        val pluto = Player("Pluto")
        val players = NilPlayers.addPlayer(pippo).players.addPlayer(mario).players.addPlayer(pluto).players

        val playerPositions = List(PlayerPosition(pippo, NormalSpace(58)),
          PlayerPosition(mario, NormalSpace(53)),
          PlayerPosition(pluto, NormalSpace(58))
        )
        val game = Game(players, GameDice, playerPositions)
        val result = game.move(mario, DiceOutcome(4, 1))

        result.gameResponse() shouldBe "Mario rolls 4, 1. Mario moves from 53 to 58. On 58 there is Pippo, who returns to 53. On 58 there is Pluto, who returns to 53"
      }
    }
  }

  "Game winning" - {
    "If any player reaches the winning space" - {
      "game should be completed" in {
        val pippo = Player("Pippo")
        val game: Game = Game(NilPlayers.addPlayer(pippo).players, GameDice)
        val gameAtPosition60 =
          (1 to 5).toList.foldLeft(game)((game, _) => game.move(pippo, DiceOutcome(6, 6)).game)
        val result = gameAtPosition60.move(pippo, DiceOutcome(1, 2)).game

        println(result.playerPositions)
        result.isGameCompleted shouldBe true
      }
    }
  }

}
