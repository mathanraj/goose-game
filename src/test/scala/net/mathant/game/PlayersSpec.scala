package net.mathant.game

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class PlayersSpec extends AnyFreeSpec with Matchers {

  "Add player" - {
    "when no players and a new player is added" - {
      "system should respond with confirmation of added player" in {
        val response: String = NilPlayers.addPlayer(Player("Pippo")).gameResponse()
        response shouldBe "players: Pippo"
      }
    }

    "with existing player and a new player is added" - {
      "system should respond with confirmation of list of players" in {
        val response = NilPlayers.addPlayer(Player("Pippo")).players.addPlayer(Player("Pluto")).gameResponse()
        response shouldBe "players: Pippo, Pluto"
      }
    }

    "with existing player and same player is added" - {
      "system should respond with error for duplicate player" in {
        val response = NilPlayers.addPlayer(Player("Pippo")).players.addPlayer(Player("Pippo")).gameResponse()
        response shouldBe "Pippo: already existing player"
      }
    }
  }

}
