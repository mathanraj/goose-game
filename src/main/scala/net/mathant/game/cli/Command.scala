package net.mathant.game.cli

import net.mathant.game.{DiceOutcome, Player}

import scala.util.Try


sealed trait Command

object Command {

  def parseCommand(command: List[String]): Command =
    command match {
      case ("help" :: _) => Help
      case ("exit" :: Nil) => Exit
      case ("add" :: "player" :: playerName :: Nil) => AddPlayer(playerName)
      case ("move" :: playerName :: Nil) => MovePlayer(playerName)
      case ("move" :: args) if (args.size == 3) =>
        val playerName = args.headOption
        val diceRolls: List[Int] = Try(args.tail.map(_.toInt)).toOption.getOrElse(List.empty[Int])
        if (playerName.isDefined && diceRolls.size == 2) {
          val player = Player(playerName.get)
          MovePlayerForGivenDiceRoll(player, DiceOutcome(diceRolls.head, diceRolls(1)))
        } else InvalidArguments
      case _ => InvalidCommand
    }

}

case object Help extends Command {
  val value =
    s"""
       |help                             - Displays help
       |add player <playerName>          - Add new player with given name. (example: 'add player Pippo')
       |move <playerName>  <dice roll>   - Supply two integers representing two die rolls. (example: 'move Pippo 2, 3')
       |move <playerName>                - System rolls the dice and moves the player.(example: 'move Pippo')
       |exit                             - Exits the Game.
       |""".stripMargin
}

case class AddPlayer(playerName: String) extends Command

case class MovePlayerForGivenDiceRoll(player: Player, diceOutCome: DiceOutcome) extends Command

case class MovePlayer(playerName: String) extends Command


case object InvalidCommand extends Command {
  val value = "Invalid command. Try again."
}

case object InvalidArguments extends Command {
  val value = "Invalid arguments. Try again."
}

case object Exit extends Command {
  val value = "exit"
}