package net.mathant.game.cli

trait IoInterface {
  def readLine(prompt: String): String

  def writeLine(line: String): Unit
}

class StandardInputOutput extends IoInterface {

  override def readLine(prompt: String): String = scala.io.StdIn.readLine(prompt)

  override def writeLine(line: String): Unit = println(line)
}
