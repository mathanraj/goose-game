package net.mathant.game.cli

import net.mathant.game._

object UserInteraction {

  private val io: IoInterface = new StandardInputOutput()
  private var command: String = ""
  private var game = Game(NilPlayers, GameDice)

  def main(mainArgs: Array[String]): Unit = {

    do {
      command = io.readLine("enter command: ")

      val (outputString, resultGame) = execute(command, game)

      io.writeLine(outputString)

      resultGame.foreach { resultGame =>
        game = resultGame
      }

      if (game.isGameCompleted) {
        io.writeLine("the game is won!!. The Game will exit.")
      }

    } while (!Exit.value.equals(command.trim) && !game.isGameCompleted)

  }

  def execute(command: String, game: Game): (String, Option[Game]) = {

    val operation = getOperationArgs(command)
    operation match {

      case AddPlayer(playerName: String) =>
        val resultingGame = game.addPlayer(Player(playerName))
        (resultingGame.gameResponse(), Some(resultingGame.game))

      case MovePlayerForGivenDiceRoll(player: Player, diceOutCome: DiceOutcome) =>
        val resultingGame = game.move(player, diceOutCome)
        (resultingGame.gameResponse(), Some(resultingGame.game))


      case MovePlayer(playerName: String) =>
        val player = Player(playerName)
        val resultingGame = game.move(player, GameDice.roll())
        (resultingGame.gameResponse(), Some(resultingGame.game))

      case Help => (Help.value, None)

      case Exit => (Exit.value, None)

      case _ => (InvalidCommand.value, None)
    }
  }

  private def getOperationArgs(command: String): Command = {
    val list: List[String] = command.split("\\s+").toList.flatMap(_.split(","))
    Command.parseCommand(list)
  }
}

