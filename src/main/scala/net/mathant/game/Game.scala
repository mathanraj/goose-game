package net.mathant.game


trait GameDef {

  val gamePlayers: PlayersDef

  val playerPositions: List[PlayerPosition]

  val dice: Dice

  def isGameCompleted: Boolean

  def validPlayer(gamePlayers: PlayersDef, player: Player): Boolean = gamePlayers.playerExists(player)

  def playerPositionsAfterPrank(player: Player, diceOutcome: DiceOutcome): PlayerPositionsResponse = {

    val mayBePlayerPosition = playerPositions.find(_.player == player)
    mayBePlayerPosition.fold {
      PlayerPositionsResponse(playerPositions, () => s"${player.name} is not a valid player for this game!")
    } { currentPosition =>
      val newPosition = move(currentPosition, diceOutcome)

      def prankPlayers(): List[(PlayerPosition, String)] = {
        playerPositions.map { otherPlayerPosition =>
          if (otherPlayerPosition.player != player && otherPlayerPosition.currentSpace == newPosition.playerPosition.currentSpace) {
            val pp = PlayerPosition(otherPlayerPosition.player, currentPosition.currentSpace)
            val response = s". On ${newPosition.playerPosition.getPosition} there is ${otherPlayerPosition.player.name}, who returns to ${currentPosition.getPosition}"
            (pp, response)
          } else {
            (otherPlayerPosition, "")
          }
        }
      }

      val playerPositionAfterPrank: List[(PlayerPosition, String)] = prankPlayers()

      val playerPositionAfterPrankAndAdvance = advancePlayerPosition(playerPositionAfterPrank.map(_._1), newPosition.playerPosition)

      def gameResponse: GameResponse = () => newPosition.gameResponse() +
        playerPositionAfterPrank.map { case (_, response) => response }.mkString

      PlayerPositionsResponse(playerPositionAfterPrankAndAdvance, gameResponse)
    }
  }

  def advancePlayerPosition(playerPositions: List[PlayerPosition],
                            destinationPosition: PlayerPosition): List[PlayerPosition] = {
    playerPositions.map { playerPosition =>
      if (playerPosition.player == destinationPosition.player) {
        PlayerPosition(destinationPosition.player, destinationPosition.currentSpace)
      } else {
        playerPosition
      }
    }
  }

  def move(playerPosition: PlayerPosition, diceOutcome: DiceOutcome): PlayerPositionResponseDef = {
    if (dice.isInvalidDiceRoll(diceOutcome)) {
      InvalidDiceRollResponse(playerPosition, diceOutcome)
    } else if (validPlayer(gamePlayers, playerPosition.player)) {
      val nextSpace = getNextSpace(playerPosition, diceOutcome)
      val p = PlayerPosition(playerPosition.player, nextSpace)

      def response: GameResponse = () => s"${playerPosition.player.name} rolls ${diceOutcome.dice1}, ${diceOutcome.dice2}." +
        s" ${playerPosition.player.name} moves from ${playerPosition.getPosition} to ${nextSpace.response}" +
        nextSpace.additionalResponse.format(playerPosition.player.name)

      PlayerPositionResponse(p, response)
    } else {
      InvalidPlayerPositionResponse(playerPosition)
    }
  }

  def getNextSpace(playerPosition: PlayerPosition, diceOutcome: DiceOutcome): Space = {

    Space.getNextSpace(
      nextPossiblePosition = playerPosition.currentSpace.position + diceOutcome.spacesToMove,
      spacesToMove = diceOutcome.spacesToMove
    )
  }
}

case class Game(gamePlayers: PlayersDef, dice: Dice, playerPositions: List[PlayerPosition]) extends GameDef {

  def addPlayer(player: Player): GameBoardResponse = {
    val playersResponse: PlayersResponse = gamePlayers.addPlayer(player)
    val game = Game(playersResponse.players, dice, playerPositions :+ PlayerPosition(player))
    GameBoardResponse(game, playersResponse.gameResponse)
  }

  def move(playerPosition: PlayerPosition): GameBoardResponse = {
    val diceOutcome = dice.roll()
    val playerPositionResponseDef = move(playerPosition, diceOutcome)
    val newPositions = advancePlayerPosition(playerPositions, playerPositionResponseDef.playerPosition)
    val game = Game(gamePlayers, dice, newPositions)

    GameBoardResponse(game, playerPositionResponseDef.gameResponse)
  }

  def move(player: Player, diceOutcome: DiceOutcome): GameBoardResponse = {
    val playerPositions = playerPositionsAfterPrank(player, diceOutcome)
    val game = Game(gamePlayers, dice, playerPositions.playerPositions)

    GameBoardResponse(game, playerPositions.gameResponse)
  }

  override def isGameCompleted: Boolean = playerPositions.exists { p => p.currentSpace == WinningSpace }
}

object Game {
  def apply(gamePlayers: PlayersDef, dice: Dice, playerPositions: List[PlayerPosition] = Nil): Game = {
    val positions = if (playerPositions.isEmpty) {
      gamePlayers.players.map(player => PlayerPosition(player))
    } else {
      playerPositions
    }
    new Game(gamePlayers, dice, positions)
  }
}

case class PlayerPositionsResponse(playerPositions: List[PlayerPosition], gameResponse: GameResponse)

case class GameBoardResponse(game: Game, gameResponse: GameResponse)
