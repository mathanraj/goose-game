package net.mathant.game

case class Player(name: String)

case class PlayersResponse(players: PlayersDef, gameResponse: GameResponse)

sealed trait PlayersDef {

  def addPlayer(player: Player): PlayersResponse

  def playerExists(player: Player): Boolean

  def players: List[Player]

  def gameResponse: GameResponse
}

case object NilPlayers extends PlayersDef {

  override def addPlayer(player: Player): PlayersResponse = {
    val players = Players(List(player))
    PlayersResponse(players, gameResponse = players.gameResponse)
  }

  override def gameResponse: GameResponse = () => s"players: None"

  override def playerExists(player: Player) = false

  override def players: List[Player] = Nil
}

case class DuplicatePlayer(player: Player, allPlayers: List[Player]) extends PlayersDef {
  override def addPlayer(player: Player): PlayersResponse = {
    val players = this
    PlayersResponse(players, gameResponse = players.gameResponse)
  }

  override def gameResponse: GameResponse = () => s"${player.name}: already existing player"

  override def playerExists(player: Player): Boolean = allPlayers.contains(player)

  override def players: List[Player] = allPlayers
}

case class Players(allPlayers: List[Player]) extends PlayersDef {

  def addPlayer(player: Player): PlayersResponse = {

    if (playerExists(player)) {
      val players = DuplicatePlayer(player, allPlayers)
      PlayersResponse(players, gameResponse = players.gameResponse)
    } else {
      val players = Players(allPlayers :+ player)
      PlayersResponse(players, gameResponse = players.gameResponse)
    }
  }

  override def gameResponse: GameResponse = () => s"players: ${allPlayers.map(_.name).mkString(", ")}"

  override def playerExists(player: Player): Boolean = allPlayers.contains(player)

  override def players: List[Player] = allPlayers
}
