package net.mathant

package object game {
  type GameResponse = () => String
}
