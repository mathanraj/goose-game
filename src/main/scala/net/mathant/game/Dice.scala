package net.mathant.game

import scala.util.Random

case class DiceOutcome(dice1: Int, dice2: Int) {
  lazy val spacesToMove: Int = dice1 + dice2
}

trait Die {
  private val maxValue = 6

  def roll(): Int = Random.nextInt(maxValue) + 1
}

trait Dice {
  def roll(): DiceOutcome

  def isInvalidDiceRoll(diceOutcome: DiceOutcome): Boolean = {
    diceOutcome.dice1 < 1 || diceOutcome.dice1 > 6 ||
      diceOutcome.dice2 < 1 || diceOutcome.dice2 > 6
  }
}

object GameDice extends Dice {
  private val die1 = new Die {}
  private val die2 = new Die {}

  def roll(): DiceOutcome = DiceOutcome(die1.roll(), die2.roll())
}