package net.mathant.game

sealed trait PlayerPositionResponseDef {
  val playerPosition: PlayerPosition
  val gameResponse: GameResponse
}

case class PlayerPositionResponse(playerPosition: PlayerPosition, gameResponse: GameResponse) extends PlayerPositionResponseDef

case class InvalidPlayerPositionResponse(playerPosition: PlayerPosition, gameResponse: GameResponse) extends PlayerPositionResponseDef

object InvalidPlayerPositionResponse {
  def apply(playerPosition: PlayerPosition): InvalidPlayerPositionResponse = {
    def response: GameResponse = () => s"${playerPosition.player.name} is not a valid player for this game!"

    InvalidPlayerPositionResponse(playerPosition, response)
  }
}

case class InvalidDiceRollResponse(playerPosition: PlayerPosition, gameResponse: GameResponse) extends PlayerPositionResponseDef

object InvalidDiceRollResponse {
  def apply(playerPosition: PlayerPosition, diceOutcome: DiceOutcome): InvalidDiceRollResponse = {
    def response: GameResponse = () => s"For player: ${playerPosition.player.name} dice roll: (${diceOutcome.dice1}, ${diceOutcome.dice2}) is not a valid dice roll for this game!"

    InvalidDiceRollResponse(playerPosition, response)
  }
}