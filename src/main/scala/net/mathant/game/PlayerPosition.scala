package net.mathant.game

case class PlayerPosition(player: Player, currentSpace: Space = StartingSpace) {

  def getPosition: String = {
    if (currentSpace == StartingSpace) "Start"
    else currentSpace.position.toString
  }
}
