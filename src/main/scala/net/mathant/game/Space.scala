package net.mathant.game

import scala.annotation.tailrec

sealed trait Space {
  val position: Int

  def response: String = s"$position"

  val additionalResponse: String = ""
}

object Space {
  def getNextSpace(nextPossiblePosition: Int, spacesToMove: Int): Space = {
    nextPossiblePosition match {
      case (63) => WinningSpace
      case (0) => StartingSpace
      case (6) => TheBridgeSpace
      case x if GooseSpace.isGooseSpace(x) => GooseSpace(x, spacesToMove)
      case x if (x > 63) => BouncedSpace(x)
      case _ => NormalSpace(nextPossiblePosition)
    }
  }
}

case object WinningSpace extends Space {
  override val position: Int = 63
  override val additionalResponse: String = ". %s Wins!!"
}

case object StartingSpace extends Space {
  override val position: Int = 0
}

case class BouncedSpace(bouncedPosition: Int) extends Space {
  override val position: Int = 63 - (bouncedPosition - 63)

  override def response: String = 63.toString

  override val additionalResponse: String = ". %1$s bounces! %1$s returns to " + position
}

case class NormalSpace(position: Int) extends Space

case object TheBridgeSpace extends Space {
  override val position: Int = 12
  override val response: String = ""
  override val additionalResponse: String = s"The Bridge. %s jumps to $position"
}


case class GooseSpace(movedPosition: Int, spacesMoved: Int) extends Space {

  import GooseSpace._

  @tailrec
  private def moveToNextGooseSpace(positionAfterMoved: Int, response: String): (Int, String) = {
    if (isGooseSpace(positionAfterMoved)) {
      val newPosition = positionAfterMoved + spacesMoved
      val newResponse = response + positionAfterMoved.toString + ", The Goose. %1$s moves again and goes to "
      moveToNextGooseSpace(newPosition, newResponse)
    } else (positionAfterMoved, response + positionAfterMoved.toString)
  }

  override val (position, additionalResponse) = moveToNextGooseSpace(movedPosition, "")

  override def response: String = ""
}

object GooseSpace {
  val gooseSpaces = List(5, 9, 14, 18, 23, 27)

  def isGooseSpace(position: Int): Boolean = gooseSpaces.contains(position)
}